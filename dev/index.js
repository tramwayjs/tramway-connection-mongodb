import {
    MongoProvider,
} from './providers';
import * as repositories from './repositories';

export default MongoProvider;
export {
    repositories,
};