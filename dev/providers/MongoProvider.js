import {Provider} from 'tramway-core-connection';
import querystring from 'querystring';
import {MongoClient, ObjectId} from 'mongodb';

const DEFAULT_PORT = 27017;

export default class MongoProvider extends Provider {

    constructor(params = {}) {
        super();
        const {
            host, 
            port = DEFAULT_PORT, 
            database,
            options,
            hosts = [],
            username,
            password,
            ssl = false,
        } = params;

        let location = `${host}:${port}`;

        if (hosts.length) {
            location = replicas.map(({host, port = DEFAULT_PORT}) => `${host}:${port}`).join(',');
        }

        let auth = '';

        if (username && null == password) {
            auth = `${username}:${password}@`;
        }

        let protocol = 'mongodb';

        if (ssl) {
            protocol = `${protocol}+srv`;
        }

        let url = `${protocol}://${auth}${location}/${database}`;

        if (options) {
            url = `${url}?${querystring.stringify(options)}`;
        }

        this.url = url;
        this.databaseName = database;
        this.client = new MongoClient(this.url, {useNewUrlParser: true});
    }

    async createConnection() {
        if (this.database) {
            return this.database;
        }

        await this.client.connect();
        this.database = this.client.db(this.databaseName);

        return this.database;
    }
    
    closeConnection() {
        this.client.close();
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * @returns {Promise<Object>}
     * 
     * @memberOf Provider
     */
    async getOne(id, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.findOne({_id: new ObjectId(id)});
    }

    /**
     * @param {string} collectionName
     * @returns {Object[]}
     * @memberOf Provider
     */
    async get(collectionName) {
        return await this.find({}, collectionName);
    }

    /**
     * @param {string | Object} conditions
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async find(conditions, collectionName) {
        await this.createConnection();

        const {limit, projection, ...options} = conditions;
        const collection = this.database.collection(collectionName);

        let cursor = collection.find(options, {projection});

        if (limit) {
            cursor = cursor.limit(limit);
        }

        return await cursor.toArray();
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * @returns {Promise<boolean>}
     * 
     * @memberOf Provider
     */
    async has(id, collectionName) {
        return !!await this.getOne(id, collectionName);
    }

    /**
     * @param {string | Object} conditions
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async count(conditions, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.count();
    }

    /**
     * @param {Object} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async create(item, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.insertOne(item);
    }

    /**
     * @param {Object[]} items
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async createMany(items, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.insertMany(items);
    }

    /**
     * @param {number|string} id
     * @param {Object} item
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async update(id, item, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.updateOne({_id: new ObjectId(id)}, {$set: item});
    }

    /**
     * @param {number|string} id
     * @param {string} collectionName
     * 
     * @memberOf Provider
     */
    async delete(id, collectionName) {
        await this.createConnection();
        const collection = this.database.collection(collectionName);

        return await collection.deleteOne({_id: new ObjectId(id)});
    }

    /**
     * Recommended to use other functions first.
     * @param {string} query
     * @param {[] | Object} values
     * 
     * @memberOf Provider
     */
    async query(query, values) {
        await this.createConnection();
        return this.database.collection(collectionName);
    }
}