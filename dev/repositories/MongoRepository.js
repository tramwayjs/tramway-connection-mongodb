import {Repository} from 'tramway-core-connection';

export default class MongoRepository extends Repository {
    /**
     * 
     * @param {MongoProvider} provider 
     * @param {Factory} factory 
     * @param {string} collectionName 
     */
    constructor(provider, factory, collectionName) {
        super(provider, factory);
        this.collectionName = collectionName;
    }

    /**
     * @param {String|Number} id
     * @returns {boolean}
     * 
     * @memberOf Repository
     */
    async exists(id) {
        return await this.provider.has(id, this.collectionName);
    }

    /**
     * @param {String|Number} id
     * @returns {Entity}
     * 
     * @memberOf Repository
     */
    async getOne(id) {
        let item = await this.provider.getOne(id, this.collectionName);
        return this.factory.create(item);
    }

    /**
     * @returns {Collection}
     * 
     * @memberOf Repository
     */
    async get() {
        let items = await this.provider.get(this.collectionName);
        return this.factory.createCollection(items);
    }

    /**
     * @param {Entity} entity
     * @returns
     * 
     * @memberOf Repository
     */
    async create(entity) {
        return await this.provider.create(entity, this.collectionName);
    }

    /**
     * @param {Object[]} items
     * 
     * @memberOf Repository
     */
    async createMany(items) {
        return await this.provider.createMany(items, this.collectionName);
    }

    /**
     * @param {Entity} entity
     * @returns
     * 
     * @memberOf Repository
     */
    async update(entity) {
        return await this.provider.update(entity.getId(), entity, this.collectionName);
    }

    /**
     * @param {String|Number} id
     * @returns
     * 
     * @memberOf Repository
     */
    async delete(id) {
        return await this.provider.delete(id, this.collectionName);
    }

    /**
     * @param {string | Object} conditions
     * @returns {Collection}
     * 
     * @memberOf Repository
     */
    async find(conditions) {
        let items = await this.provider.find(conditions, this.collectionName);
        return this.factory.createCollection(items);
    }

    /**
     * @param {string | Object} conditions
     * 
     * @memberOf Repository
     */
    async count(conditions) {
        return await this.provider.count(conditions, this.collectionName);
    }
}