const assert = require('assert');
const utils = require('tramway-core-testsuite');
const lib = require('../index.js');
var describeCoreClass = utils.describeCoreClass;
var describeFunction = utils.describeFunction;

describe("Simple acceptance tests to ensure library returns what's promised.", function(){
    describe("Should return a proper 'MongoProvider' class", describeCoreClass(
        lib.default, 
        "MongoProvider", 
        [],
        ["get", "getOne", "has", "create", "createMany", "update", "delete", "find", "count", "query", "createConnection", "closeConnection"]
    ));

    describe("Should return an object for repositories.", function(){
        it("Should return an object for repositories.", function(){
            assert.strictEqual(typeof lib.repositories, "object");
        });
        it("There should be the same services as in the previous version", function(){
            assert.deepEqual(Object.keys(lib.repositories), ["MongoRepository"]);
        });
        describe("Should return a proper 'MongoRepository' class", describeCoreClass(
            lib.repositories.MongoRepository, 
            "MongoRepository", 
            [],
            ["exists", "getOne", "get", "create", "createMany", "update", "delete", "find", "count"]    
        ));
    });
});