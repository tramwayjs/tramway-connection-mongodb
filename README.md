Tramway MongoProvider is a simple Provider add-on to simplify the process of implementing calls to mongodb databases and provides an async/await wrapper on the `mongodb` module.

# Installation:
1. `npm install tramway-connection-mongodb --save`

# Example project
https://gitlab.com/tramwayjs/tramway-connection-example

# Documentation

## Recommended Folder Structure in addition to Tramway
- config
- providers
- repositories
- entities

## MongoProvider
The `MongoProvider` is a derived `Provider` which follows the same interface.

### Configuration parameters
| Parameter | Type | Required | Default | Usage |
| --- | --- | --- | --- | --- |
| host | string | no | localhost | The host of your mongodb database |
| database | string | yes | | The name of the default database to connect to |
| port | number | no | 27017 | The port for the database |
| username | string | no | | The connection username for the database |
| password | string | no | | The connection password for the database |
| hosts | array: [{host: string, port: number}] | no | | The connection password for the database |
| options | Object | no | | [Options object](https://docs.mongodb.com/manual/reference/connection-string/#connection-string-options) that would be passed to Mongo at connection time |
| ssl | bool | no | false | Determines if the connection protocol should enforce ssl via `mongodb+srv://` |

## Getting started
It is recommended to make a config file with the core mongodb configuration and make an extension class to handle it before injecting that extension class into the Repository to work with the rest of Tramway's built-in features. The alternative to the extension class is calling the config parameters with the Repository every time the repository is used instead of just importing the provider to the repository.

In your config folder add a 'mongodb' file.

mongodb.js:
```javascript
export default {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "base",
};
```

Note, the same can be achieved using your environment variables and passing the declaration from your instantiated .env to the placeholders in the above example.

To get the most of it, pass the provider to your `Repository` class.

```javascript
import MongoProvider, {repositories} from 'tramway-connection-mongodb';
import options from '../config/mongodb.js';

const {MongoRepository} = repositories;

new MongoRepository(new MongoProvider(options), new Factory());
```

The following can also be easily achieved with dependency injection using `tramway-core-dependency-injector`.

## Exposed Methods with this Library

### Provider

Note, the extended `MongoProvider` expects an additional parameter `collectionName` on most methods. Using the `MongoRepository` handles this for you. In addition, bulk inserts are possible.

| Function | Availability |
| ----- | ----- |
| ```getOne(id: any, collectionName: string)``` | Available |
| ```getMany(ids: any[], collectionName: string)``` | Unavailable |
| ```get(collectionName: string)``` | Available |
| ```find(conditions: string/Object, collectionName: string)``` | Available |
| ```has(id: any, collectionName: string)``` | Available |
| ```hasThese(ids : any[], collectionName: string)``` | Unavailable |
| ```count(conditions: any, collectionName: string)``` | Available |
| ```create(item: Entity/Object, collectionName: string)``` | Available |
| ```createMany(item: Entity/Object[], collectionName: string)``` | Additional, creates a transaction for bulk inserts |
| ```update(id: any, item: Entity/Object, collectionName: string)``` | Available |
| ```delete(id: any, collectionName: string)``` | Available |
| ```deleteMany(ids : any[], collectionName: string)``` | Unavailable |
| ```query(query: string/Object, values: Object)``` | Available |

### Repository

| Function | Usability |
| --- | --- |
| ```exists(id: any)``` | Usable |
| ```getOne(id: any)``` | Usable |
| ```get()``` | Usable |
| ```create(entity: Entity)``` | Usable |
| ```createMany(entities: Entity[])``` | Additional |
| ```update(entity: Entity)``` | Usable |
| ```delete(id: any)``` | Usable |
| ```find(condtions: string/Object)``` | Usable |
| ```getMany(ids: any[])``` | Unusable |
| ```count(conditions)``` | Usable |

### Advanced Connection Options

For most basic connections, a `mongo.js` file like this will be sufficient:

```javascript
export default {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "base",
};
```

If the basic connection had authentication:

```javascript
export default {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "base",
    "username": "user",
    "password": "password",
};
```

However, Mongo can also be set up as a Replica Set or Sharded Cluster.

To connect to a replica set:

```javascript
export default {
    "database": "base",
    "hosts": [
        {"host": "db0.example.com", "port": 27017},
        {"host": "db1.example.com", "port": 27017},
        {"host": "db2.example.com", "port": 27017},
    ],
    "options": {
        "replicaSet": "replicaSetName"
    }
};
```

...with authentication:

```javascript
export default {
    "database": "base",
    "hosts": [
        {"host": "db0.example.com", "port": 27017},
        {"host": "db1.example.com", "port": 27017},
        {"host": "db2.example.com", "port": 27017},
    ],
    "options": {
        "replicaSet": "replicaSetName"
    },
    "username": "user",
    "password": "password",
};
```

To connect to a sharded cluster:

```javascript
export default {
    "database": "base",
    "hosts": [
        {"host": "dbs0.example.com", "port": 27017},
        {"host": "dbs1.example.com", "port": 27017},
        {"host": "dbs2.example.com", "port": 27017},
    ],
};
```

...with authentication:

```javascript
export default {
    "database": "base",
    "hosts": [
        {"host": "dbs0.example.com", "port": 27017},
        {"host": "dbs1.example.com", "port": 27017},
        {"host": "dbs2.example.com", "port": 27017},
    ],
    "username": "user",
    "password": "password",
};
```